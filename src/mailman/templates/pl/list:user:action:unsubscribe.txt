Potwierdzenie rezygnacji z subskrypcji adresu e-mail

Witam, tu serwer GNU Mailman w domenie $.

Otrzymaliśmy prośbę o anulowanie subskrypcji dla adresu e-mail

    $user_email

Zanim GNU Mailman będzie mógł anulować subskrypcję, musisz najpierw potwierdzić swoją prośbę.
Możesz to zrobić po prostu odpowiadając na tę wiadomość.

Lub możesz dołączyć następującą linię - i tylko następującą linię
w wiadomości wysyłanejna adres $request_email:

    confirm $token

Zauważ, że zwykłe wysłanie `odpowiedzi' na tę wiadomość powinno działać z
większości czytników poczty.

Jeśli nie chcesz zrezygnować z subskrypcji tego adresu e-mail, po prostu zignoruj tę wiadomość.
Jeśli uważasz, że zostałeś złośliwie wypisany z listy,
lub masz inne pytania, możesz skontaktować się z

    $owner_email
